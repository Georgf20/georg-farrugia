﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPad : MonoBehaviour {

	// to check if player is on trigger
	bool onTop;

	// amount of increase in jump
	public float multiplier;

	// on trigger, mark boolean as true and call jump function
	void OnTriggerEnter2D(Collider2D other) {
		onTop = true;
		jump (other);
	}

	// on exit, mark trigger to false and make jump force to original force
	void OnTriggerExit2D(Collider2D player) {
		onTop = false;
		PlayerController controller = player.GetComponent<PlayerController> ();
		controller.JumpForce = 2000;
	}

	// increase the jump force by the amount stated
	void jump(Collider2D player) {
		if (onTop = true) {
			PlayerController controller = player.GetComponent<PlayerController> ();
			controller.JumpForce *= multiplier;
		}
	}
}
















