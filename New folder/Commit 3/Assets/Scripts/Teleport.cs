﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour {

	// to insert where to teleport
	public Transform Destination;

	// Getting the position of the character, transforming it into the destination's position
	void OnTriggerEnter2D(Collider2D other) {
		other.gameObject.transform.position = Destination.position;
	}
}
