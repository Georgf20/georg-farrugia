﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	private Rigidbody2D myRigidBody;
	// speed of the character
	public float movementSpeed;
	// jumpforce of the car
	public float JumpForce;
	// do disable double jump
	private bool Jump;

	// Use this for initialization
	void Start () {
		myRigidBody = GetComponent<Rigidbody2D> ();
		Jump = false;
	}

	// Update is called once per frame
	void Update () {
		// He has to touch the ground before jumping again
		if (Input.GetKeyDown (KeyCode.Space) && !Jump) {
			myRigidBody.AddForce (new Vector2 (0, JumpForce));
			Jump = true;
		}
			
		// to set him move left and right
		float horizontal = Input.GetAxis ("Horizontal");

		myRigidBody.velocity = new Vector2 (horizontal * movementSpeed, myRigidBody.velocity.y);
		// calling the flip method
		flip ();
	}

	private void flip() 
	{
		// if moving right, dont flip him
		if (myRigidBody.velocity.x > 0)
		{
			GetComponent<SpriteRenderer>().flipX = false;
	    }

		// if moving left, flip him
		else if (myRigidBody.velocity.x < 0) 
		{
			GetComponent<SpriteRenderer> ().flipX = true;
		}
	}

	// to disable double jump
	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.name == "Ground") {
			Jump = false;
		}
			
	}
}
