﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour {

	public Slider Volume;
	AudioSource myMusic;
	AudioSource audio;

	// making mySoundMangager null
	static SoundManager mySoundManager = null;


	void Update() {
		// Controlling volume of sound from the slider
		if (Volume != null) {
			myMusic.volume = Volume.value;
		}
	}

	void Awake()
	{
		if (mySoundManager != null)
		{
			//destroy the new game sound object created
			Destroy(this.gameObject);
		}
		else 
		{
			//mySoundManager is filled with my FIRST Sound Object
			mySoundManager = this;
			myMusic = GetComponent<AudioSource> ();

			if (GameObject.Find ("VolumeSlider") != null) {
				Volume = GameObject.Find ("VolumeSlider").GetComponent<Slider> ();
				Volume.value = myMusic.volume;

				Volume.onValueChanged.AddListener ((VolumeSlider) => {
					myMusic.volume = Volume.value;
				});
			}
		}
			//DontDestroyOnLoad = do not destroy the gameObject when loading a new scene
			DontDestroyOnLoad(this.gameObject);
	}

	public void mute () {
		//audioSource.mute = !audioSource.mute;
	}
}


