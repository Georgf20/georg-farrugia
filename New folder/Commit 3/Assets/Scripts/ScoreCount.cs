﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ScoreCount : MonoBehaviour {

	public static int score = 0;
	Text scoreCount;

	// Use this for initialization
	void Start () {
		score = 0;
		scoreCount = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		scoreCount.text = "Score: " + score;
		scoreCount.transform.position = new Vector3 (700, 850, 0);
	}
}
