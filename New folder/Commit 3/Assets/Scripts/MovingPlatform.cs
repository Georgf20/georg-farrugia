﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {

	// saving pos A
	private Vector3 posA;

	// saving pos B
	private Vector3 posB;

	// saving the next position
	private Vector3 nextPos;

	// To enter the value from unity
	[SerializeField]
	private float speed;

	// To enter the value from unity
	[SerializeField]
	private Transform childTransform;

	// To enter the value from unity
	[SerializeField]
	private Transform transformB;

	// Use this for initialization
	void Start () {
		// storing the posA in the childTransform and pos B into transformB
		posA = childTransform.localPosition;
		posB = transformB.localPosition;
		nextPos = posB;
	}
	
	// Update is called once per frame
	void Update () {
		Move();
	}

	private void Move () {
		// To move the platform towards the positionB
		childTransform.localPosition = Vector3.MoveTowards (childTransform.localPosition, nextPos, speed * Time.deltaTime);

		// If it is 0.1 away do ChangePosition
		if (Vector3.Distance (childTransform.localPosition, nextPos) <= 0.1) {
			ChangePosition ();
		}
	}

	private void ChangePosition() {
		// Sets the next position to either a or B, if equal a switches it to B
		nextPos = nextPos != posA ? posA : posB;
	}
}
