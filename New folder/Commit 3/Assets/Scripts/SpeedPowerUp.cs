﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedPowerUp : MonoBehaviour {

	// amount of speed to multiply
	public float multiplier;
	// duration of powerup
	public float duration = 4f;

	// on trigger, call the pick up method
	void OnTriggerEnter2D(Collider2D other) {
		if (other.CompareTag ("Player")) {
			StartCoroutine (Pickup (other));
		}
	}

	// Coroutine since we have to go back to original speed after 4s
	IEnumerator Pickup(Collider2D player) {
		// getting the speed from the PlayerController Script
		PlayerController controller = player.GetComponent<PlayerController> ();

		// multiplying the speed
		controller.movementSpeed *= multiplier;

		// to make the power up disappear when it is triggered not after the effect stops.
		GetComponent<SpriteRenderer> ().enabled = false;
		GetComponent<Collider2D> ().enabled = false;

		// After 4s, go back to original speed
		yield return new WaitForSeconds (duration);

		controller.movementSpeed /= multiplier;
		Destroy (gameObject);
	}
}
