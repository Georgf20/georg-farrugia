﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

	public static bool GamePaused = false;

	public GameObject PauseMenuUI;

	// When the sprite is clicked, check if it is resumed or pause depending on the boolean
	void OnMouseDown () {
		if (GamePaused) {
			Resume ();
		} else {
			Pause ();	
		}
	}
		
	public void Resume() {
		// disable the gui
		PauseMenuUI.SetActive (false);
		// continue the timer
		Time.timeScale = 1f;
		// turn boolean off
		GamePaused = false;
	}

	 void Pause () {
		// make the gui visable
		PauseMenuUI.SetActive (true);
		// pause the timer
		Time.timeScale = 0f;
		// set the boolean to true
		GamePaused = true;
	}

	public void LoadMenu () {
		// set timer 
		Time.timeScale = 1f;
		// display menu scene
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex - 3);
	}

	public void QuitGame() {
		// quit the game
		UnityEditor.EditorApplication.isPlaying = false;
	}
}
