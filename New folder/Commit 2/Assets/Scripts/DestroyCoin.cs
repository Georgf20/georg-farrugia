﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestroyCoin : MonoBehaviour {

	// Setting a game object to mark the coin as obj to destroy
	public GameObject objToDestroy;

	void OnTriggerEnter2D(Collider2D other) {
		ScoreCount.score += 1;
		// on trigger, destroy the coin
		Destroy (objToDestroy);
	}
}


