﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour {

	public Transform Destination;

	void OnTriggerEnter2D(Collider2D other) {

		Debug.Log ("An object has collided.");
		other.gameObject.transform.position = Destination.position;
	}
}
