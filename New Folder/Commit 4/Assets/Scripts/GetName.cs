﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetName : MonoBehaviour {
	// saving the name in this variable
    string GetplayerName;
	// allocating where to update the text
	public Text myText;

	// Use this for initialization
	void Start () {
		// calling the below function
		getText ();

	}
	
	public void getText() {
		// setting the getplayername in the player pref of username which was stored in the insert name script
		GetplayerName = PlayerPrefs.GetString ("Username");
		myText.text = "Congratulations" + " " + GetplayerName + " on winning the game would you like to try again?";
	}
}
