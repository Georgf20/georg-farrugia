﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour {
	// assigning slider value
	public Slider Volume;
	// assigning which sound 
	public AudioSource musicSource;
	// making instance to null
	public static SoundManager instance = null;

	void Update() {
		// Controlling volume of sound from the slider
		if (Volume != null) {
			musicSource.volume = Volume.value;
		}
	}

	void Awake() 
	{
		// if instance is null than start playing this song.
		if (instance == null)
			instance = this;
		else if (instance != this)
		// if it is not null, than destroy this song
			Destroy (this.gameObject);
		// dont destroy the song between scenes
		DontDestroyOnLoad (gameObject);
		
	}

	//function to play music
	public void PlayMusic(AudioClip clip) {
		musicSource.clip = clip;
		musicSource.Play ();
	}
	// function to pause music
	public void StopMusic (AudioClip clip) {
		musicSource.clip = clip;
		musicSource.Pause();
	}
}
