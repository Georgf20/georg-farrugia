﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TimerCountDown : MonoBehaviour {
	// Create text
	Text text;
	// amount of time given
	public static float timeLeft = 20f;

	// Use this for initialization
	void Start () {
		// get the text
		timeLeft = 20f;
		text = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		// do the 20 - the time
		timeLeft -= Time.deltaTime;

		// when it is smaller than 0, keep it 0 and change the scene
		if (timeLeft < 0) {
			timeLeft = 0;
			SceneManager.LoadScene ("Defeat");
		}
		// To display the text
		text.text = "Time Left: " + Mathf.Round (timeLeft);
		// position of the text
		text.transform.position = new Vector3 (850, 850, 0);
	}
}
