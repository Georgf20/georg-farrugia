﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelsScript : MonoBehaviour {

	public void QuitGame() {
		UnityEditor.EditorApplication.isPlaying = false;
	}

	public void LoadInstructions () {
		SceneManager.LoadScene ("Instructions");
	}

	public void LoadMainMenu () {
		SceneManager.LoadScene ("MainMenu");
	}

	public void LoadEnterName () {
		SceneManager.LoadScene ("Insert Name");
	}

	public void LoadLevel1 () {
		SceneManager.LoadScene ("Level1");
	}

	public void LoadLevel2 (){
		SceneManager.LoadScene ("Level2");
	}
}


