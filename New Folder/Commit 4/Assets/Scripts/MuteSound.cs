﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuteSound : MonoBehaviour {
	// getting the song
	public AudioClip BackgroundMusic;

	// call the function from sound manager to stop music
	public void OnMouseDown() {
		SoundManager.instance.StopMusic (BackgroundMusic);
		}
}
			
	

