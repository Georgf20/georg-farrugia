﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UmuteSound : MonoBehaviour {
	// setting which song
	public AudioClip BackgroundMusic;
	// getting the function from the soundmanager script to play music
	public void OnMouseDown() {
		SoundManager.instance.PlayMusic (BackgroundMusic);
	}
}
