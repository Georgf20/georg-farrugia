﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spikes : MonoBehaviour {

	// If the character touches the spikes or goes out of the map, display defeat screen
	void OnTriggerEnter2D(Collider2D other) {
		SceneManager.LoadScene("Defeat");
	}
}
