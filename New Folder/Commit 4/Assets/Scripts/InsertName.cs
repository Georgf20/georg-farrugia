﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InsertName : MonoBehaviour {
	// seeing which inputfield
	public InputField nameField;
	// variable to store the name
    static string playerName;

	void Start () {
		// setting the value of playername in username
		playerName = PlayerPrefs.GetString("Username");
		// setting the value of the input field in playername
		nameField.text = playerName;


	}

	public void ContinuePressed () {
		// if continue is pressed, put the input field in the variable player name
		playerName = nameField.text;
		// store playername in username
		PlayerPrefs.SetString("Username", playerName);
	}
}