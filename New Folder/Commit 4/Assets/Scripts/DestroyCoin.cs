﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DestroyCoin : MonoBehaviour {

	public AudioClip impact;
	void Start () {
	}

	// Setting a game object to mark the coin as obj to destroy
	public GameObject objToDestroy;

	void OnTriggerEnter2D(Collider2D other) {
		ScoreCount.score += 1;
		AudioSource.PlayClipAtPoint(impact, transform.position);
		// on trigger, destroy the coin
		Destroy (objToDestroy);
	
	
		if (ScoreCount.score == 5) {
			SceneManager.LoadScene ("AfterLevel1");
		}
	}
}


